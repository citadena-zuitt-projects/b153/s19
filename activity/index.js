let getCube = 4 ** 3
console.log(`The cube of 4 is ${getCube}.`);

let address = ["Block 11- Lot 17 ", "Clarkia Street ", "Palmera Homes", "Angeles City"]
let [houseNumber, street, subdivision, city] = address

console.log(`I live in ${houseNumber}, ${street}, ${subdivision}, ${city}.`)

let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in"
};

let { name, type, weight, length } = animal
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${length}.`)

let num = [ 1, 2, 3, 4, 5 ]

num.forEach(x => console.log(x));

let reduceNumber = (previousNum, currentNum) => previousNum + currentNum;

console.log(num.reduce(reduceNumber));